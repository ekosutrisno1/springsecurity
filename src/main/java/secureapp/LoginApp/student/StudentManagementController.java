package secureapp.LoginApp.student;

import java.util.Arrays;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * StudentManagementController
 */
@RestController
@RequestMapping("management/api/siswa")
public class StudentManagementController {

  /**
   * hasRole/hasAnyRole isinya adalah role dari user yang masuk sedanglan
   * hasAuthoriry/hasAnyAuthority isinya adalah user permission / izi akses
   */

  private static final List<Student> STUDENT = Arrays.asList(new Student(1, "Eko Sutrisno"),
      new Student(2, "Muhammad Iqbal"), new Student(3, "Alfia Husna"));

  @GetMapping
  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')")
  public List<Student> getAllStudent() {
    System.out.print("Get All : ");
    return STUDENT;
  }

  @PostMapping
  @PreAuthorize("hasAuthority('student:write')")
  public void newStudent(@RequestBody Student student) {
    System.out.println("New Student: " + student);
  }

  @DeleteMapping(path = "{studentId}")
  @PreAuthorize("hasAuthority('student:write')")
  public void deleteStudent(@PathVariable("studentId") Integer studentId) {
    System.out.println("Delete Student: " + studentId);
  }

  @PutMapping(path = "{studentId}")
  @PreAuthorize("hasAuthority('student:write')")
  public void updateStudent(@PathVariable("studentId") Integer studentId, @RequestBody Student student) {
    System.out.printf("Update Student:  %s and %s", student, student);
  }
}