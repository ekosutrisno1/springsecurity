package secureapp.LoginApp.student;

/**
 * Student
 */
public class Student {

  private final Integer studentId;
  private final String studentNamae;

  public Student(Integer studentId, String studentNamae) {
    this.studentId = studentId;
    this.studentNamae = studentNamae;
  }

  public Integer getStudentId() {
    return studentId;
  }

  public String getStudentNamae() {
    return studentNamae;
  }

  @Override
  public String toString() {
    return "Student [studentId=" + studentId + ", studentNamae=" + studentNamae + "]";
  }

}