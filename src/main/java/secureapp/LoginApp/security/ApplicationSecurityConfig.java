package secureapp.LoginApp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
// import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
// import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static secureapp.LoginApp.security.ApplicationUserRole.*;
// import static secureapp.LoginApp.security.ApplicationUserPermission.*;

import java.util.concurrent.TimeUnit;

/**
 * ApplicationSecurityConfig
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder passwordEncoder;

  @Autowired
  public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests()

        .antMatchers("/", "index", "/css/*", "/js/*").permitAll()

        .antMatchers("/api/**").hasRole(STUDENT.name())

        .anyRequest().authenticated()

        .and().formLogin().loginPage("/login").permitAll().defaultSuccessUrl("/dashboard", true)

        .and().rememberMe().tokenValiditySeconds((int) TimeUnit.MINUTES.toSeconds(60)).key("sangatrahasia")

        .and().logout().logoutUrl("/logout")

        .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "GET")).clearAuthentication(true)
        .invalidateHttpSession(true)

        .deleteCookies("JSESSIONID", "remember-me").logoutSuccessUrl("/login");
  }

  @Override
  @Bean
  protected UserDetailsService userDetailsService() {
    UserDetails ekoSutrisno = User.builder().username("ekosutrisno").password(passwordEncoder.encode("password"))
        // .roles(STUDENT.name())
        .authorities(STUDENT.getGrantedAuthorirties()).build();

    UserDetails aminAnjari = User.builder().username("aminanjari").password(passwordEncoder.encode("password1"))
        // .roles(ADMIN.name())
        .authorities(ADMIN.getGrantedAuthorirties()).build();

    UserDetails agusSalim = User.builder().username("agussalim").password(passwordEncoder.encode("password2"))
        // .roles(ADMINTRAINEE.name())
        .authorities(ADMINTRAINEE.getGrantedAuthorirties()).build();

    return new InMemoryUserDetailsManager(ekoSutrisno, aminAnjari, agusSalim);
  }

}

/*
 * .antMatchers(HttpMethod.DELETE,
 * "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
 * .antMatchers(HttpMethod.POST,
 * "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
 * .antMatchers(HttpMethod.PUT,
 * "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
 * .antMatchers("/management/api/**").hasAnyRole(ADMIN.name(),
 * ADMINTRAINEE.name())
 */