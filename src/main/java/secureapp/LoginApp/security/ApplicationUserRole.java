package secureapp.LoginApp.security;

import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static secureapp.LoginApp.security.ApplicationUserPermission.*;

/**
 * ApplicationUserRole
 */
public enum ApplicationUserRole {
  STUDENT(Sets.newHashSet()), ADMIN(Sets.newHashSet(COURSE_READ, COURSE_WRITE, STUDENT_READ, STUDENT_WRITE)),
  ADMINTRAINEE(Sets.newHashSet(COURSE_READ, STUDENT_READ));

  private final Set<ApplicationUserPermission> permission;

  ApplicationUserRole(Set<ApplicationUserPermission> permission) {
    this.permission = permission;
  }

  /**
   * @return the permission
   */
  public Set<ApplicationUserPermission> getPermission() {
    return permission;
  }

  public Set<SimpleGrantedAuthority> getGrantedAuthorirties() {
    Set<SimpleGrantedAuthority> permission = getPermission().stream()
        .map(permissions -> new SimpleGrantedAuthority(permissions.getPermission())).collect(Collectors.toSet());

    permission.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
    return permission;
  }
}