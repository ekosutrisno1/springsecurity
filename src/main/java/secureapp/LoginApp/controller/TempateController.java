package secureapp.LoginApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TempateController
 */
@Controller
@RequestMapping("/")
public class TempateController {

  @GetMapping("index")
  public String indexnPage() {
    return "index";
  }

  @GetMapping("login")
  public String loginPage() {
    return "login";
  }

  @GetMapping("dashboard")
  public String dashboardView() {
    return "dashboard";
  }
}